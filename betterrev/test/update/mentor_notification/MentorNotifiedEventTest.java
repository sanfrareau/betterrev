package update.mentor_notification;

import akka.actor.ActorRef;
import models.PullReview;
import models.PullReviewEvent;
import models.PullReviewEventType;
import models.PullReviewTest;

import org.junit.Rule;
import org.junit.Test;

import scala.concurrent.Await;
import scala.concurrent.Future;
import scala.concurrent.duration.Duration;
import update.BetterrevActor;
import update.GreenMailRule;
import update.pullrequest.AbstractPullRequestImporterTest;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import java.io.IOException;
import java.util.List;

import static java.util.concurrent.TimeUnit.SECONDS;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * MentorNotifiedEventTest
 */
public class MentorNotifiedEventTest extends AbstractPullRequestImporterTest {

    @Rule
    public final GreenMailRule mailRule = new GreenMailRule();

    @Test
    public void pullReviewGenerationNotifiesMentorByEmail() throws Exception {
        PullReview review = PullReviewTest.createTestInstance();
        PullReviewEvent pullReviewEvent = review.pullReviewEvents.get(0);
        messageNotificationActor(pullReviewEvent);

        Future<PullReviewEvent> response = actorRule.expectMsg(PullReviewEvent.class, SECONDS.toMillis(10));
        Await.result(response, Duration.Inf());

        List<MimeMessage> receivedMessages = mailRule.getReceivedMessages();
        checkEmail(review, receivedMessages);
    }

    private void checkEmail(PullReview review, List<MimeMessage> receivedMessages) throws MessagingException, IOException {
        assertEquals(1, receivedMessages.size());
        MimeMessage message = receivedMessages.get(0);
        assertEquals("RFR: " + review.name, message.getSubject());
        
        checkEmailContent(review, message);
    }

    private void checkEmailContent(PullReview review, MimeMessage message) throws MessagingException, IOException {
        assertTrue(message.getContentType().contains("text"));
        String body = (String) message.getContent();
        assertTrue(body.contains(review.description));
        assertTrue(body.contains(review.pullRequestUrl()));
        assertTrue(body.contains(review.requester.openjdkUsername));
    }

    @Test
    public void irrelevantEventsDontNotifyMentor() throws Exception {
        PullReviewEvent pullReviewEvent = new PullReviewEvent(PullReviewEventType.TERMINATED);
        Future<PullReviewEvent> response = messageNotificationActor(pullReviewEvent);

        PullReviewEvent reply = Await.result(response, Duration.Inf());
        assertEquals(PullReviewEventType.TERMINATED, reply.pullReviewEventType);

        actorRule.expectNoMsgWithinTimeout(PullReviewEvent.class, 200);
    } 

    private Future<PullReviewEvent> messageNotificationActor(PullReviewEvent pullReviewEvent) {
        ActorRef mentorNotificationActor = actorRule.actorOf(MentorNotificationActor.class);
        eventStream.subscribe(mentorNotificationActor, PullReviewEvent.class);
        Future<PullReviewEvent> first = actorRule.expectMsg(PullReviewEvent.class, SECONDS.toMillis(10));
        eventStream.publish(pullReviewEvent);
        return first;
    }
    
}
