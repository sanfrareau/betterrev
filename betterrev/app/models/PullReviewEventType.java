package models;

public enum PullReviewEventType {
    /** A user has created a pull review on bitbucket */
    PULL_REVIEW_GENERATED(true),

    /**
     * A user has created a modified this pull review on bitbucket by changing
     * the code
     */
    PULL_REVIEW_MODIFIED(true),

    /** We've notified the mentor that they need to review the Pull Review */
    MENTOR_NOTIFICATION_REQUEST(false),

    /** We've notified the mentor that they need to review the Pull Review */
    MENTOR_NOTIFIED(true),

    /** we've generated a webrev and put it up online */
    WEBREV_GENERATED(true),

    /** Upstream has merged this Pull Review into our master */
    MERGED(true),

    /** Upstream has merged this Pull Review into their codebase */
    APPROVED(true),

    /** This pull review has been rejected */
    REJECTED(true),

    /** Pull Review was closed in bitbucket without going into openjdk */
    TERMINATED(true);

    private final boolean visible;

    private PullReviewEventType(boolean visible) {
        this.visible = visible;
    }

    /**
     * @return whether this event is visible to the end user or not.
     */
    public boolean isVisible() {
        return visible;
    }

}