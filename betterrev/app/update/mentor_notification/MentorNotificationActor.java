package update.mentor_notification;

import static models.PullReviewEventType.MENTOR_NOTIFIED;

import java.util.Iterator;
import java.util.Properties;
import java.util.Set;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import models.Mentor;
import models.PullReviewEvent;
import models.PullReviewEventType;
import play.Configuration;
import play.Play;
import play.api.templates.Html;
import update.BetterrevActor;
import views.html.emails.PullReviewCreated;

/**
 * MentorNotificationActor
 */
public class MentorNotificationActor extends BetterrevActor {
    
    private static final String FROM_EMAIL = " betterrev@googlegroups.com";
    private static final String HTML_MIME_TYPE = "text/html";
    
    private Session session;

    public MentorNotificationActor() {
        session = makeSession();
    }

    @Override
    public void onReceive(Object message) throws Exception {
        if (!(message instanceof PullReviewEvent)) {
            return;
        }

        PullReviewEvent request = (PullReviewEvent) message;

        if (request.pullReviewEventType != PullReviewEventType.PULL_REVIEW_GENERATED) {
            return;
        }
        
                
        sendEmailMessage(request);
        publishResponse(request);
    }

    private void publishResponse(PullReviewEvent request) {
        PullReviewEvent response = new PullReviewEvent(MENTOR_NOTIFIED, request.pullReview);
        eventStream().publish(response);
    }

    private void sendEmailMessage(PullReviewEvent request) throws MessagingException, AddressException {
        MimeMessage emailMessage = createEmail(request);
        renderEmailContent(request, emailMessage);
        Transport.send(emailMessage);
    }

    private MimeMessage createEmail(PullReviewEvent request) throws MessagingException, AddressException {
        MimeMessage emailMessage = new MimeMessage(session);
        emailMessage.setFrom(new InternetAddress(FROM_EMAIL));
        setRecipients(request.pullReview.mentors, emailMessage);
        return emailMessage;
    }

    private void setRecipients(Set<Mentor> mentors, MimeMessage emailMessage) throws MessagingException, AddressException {
        InternetAddress[] addresses = new InternetAddress[mentors.size()];
        Iterator<Mentor> it = mentors.iterator();
        for (int i = 0; it.hasNext(); i++) {
            Mentor mentor = it.next();
            addresses[i] = new InternetAddress(mentor.email);
        }
        emailMessage.setRecipients(Message.RecipientType.TO, addresses);
    }

    private void renderEmailContent(PullReviewEvent request, MimeMessage emailMessage) throws MessagingException {
        emailMessage.setSubject("RFR: " + request.pullReview.name);
        Html body = PullReviewCreated.render(request.pullReview);
        emailMessage.setContent(body.toString(), HTML_MIME_TYPE);
    }

    private Session makeSession() {
        Configuration conf = Play.application().configuration();
        Properties props = new Properties();
        loadPropertyFromConfiguration(conf, props, "mail.smtp.host");
        loadPropertyFromConfiguration(conf, props, "mail.smtp.port");
        return Session.getDefaultInstance(props);
    }

    private void loadPropertyFromConfiguration(Configuration from, Properties to, String propertyName) {
        String value = from.getString(propertyName);
        to.setProperty(propertyName, value);
    }
    
}
