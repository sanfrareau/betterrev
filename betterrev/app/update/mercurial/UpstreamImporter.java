package update.mercurial;

import akka.actor.UntypedActor;
import play.Logger;

/**
 * Constantly polls openjdk repos in order to pull in changes.
 */
public class UpstreamImporter extends UntypedActor {

    @Override
    public void onReceive(Object message) throws Exception {
        Logger.info("Polling openjdk for changes");
        // TODO: #21
    }

}
